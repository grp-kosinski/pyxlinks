from setuptools import setup

setup(
    name='pyxlinks',
    version='0.0.4dev',
    author='Jan Kosinski',
    author_email='jan.kosinski@embl.de',
    packages=['pyxlinks'
              ],
    scripts=[
        'scripts/compare.py',
        'scripts/concat.py',
        'scripts/expected_from_pdb.py',
        'scripts/make_input_for_crosslinkviewer.py',
        'scripts/make_stats.py',
        'scripts/merge_datasets.py',
        # 'scripts/mxdb_to_xquest.py',
        'scripts/rename.py',
        'scripts/renumber.py',
        'scripts/show_protein_names.py',
        # 'scripts/train.py',
        'scripts/xquest_summary.py',
        # 'scripts/xquest_to_restraints.py',
        'scripts/unique_by_resi.py'
             ],
    url='none',
    license='LICENSE.txt',
    description='Toolbox for dealing with cross-links.',
    # long_description=open('README').read(),
)

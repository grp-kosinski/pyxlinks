#!/usr/bin/python
from optparse import OptionParser

import pyxlinks

def main():
    usage = "usage: %prog [options] xquest_filenames > outfilename"
    parser = OptionParser(usage=usage)

    parser.add_option("--do_not_add_extra_fields", action="store_true", dest="do_not_add_extra_fields", default=False,
                      help="Whether to add columns with xlinker and source [default: %default]")

    (options, args) = parser.parse_args()

    xquest_filenames = args[:]

    if options.do_not_add_extra_fields:
        add_extra_fields = False
    else:
        add_extra_fields = True

    xlinks_sets = []

    for filename in xquest_filenames:
        xlink_set = pyxlinks.XlinksSet(filename, add_extra_fields=add_extra_fields)

        xlinks_sets.append(xlink_set)

    xlinks_sets_merged = sum(xlinks_sets)

    xlinks_sets_merged.print_out()

if __name__ == '__main__':
    main()
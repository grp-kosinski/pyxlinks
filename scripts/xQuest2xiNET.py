#!/usr/bin/python
from optparse import OptionParser

import pyxlinks

replace_dict = {
    'AbsPos1': 'LinkPos1',
    'AbsPos2': 'LinkPos2',
    'ld-score': 'Score',
    'ld-Score': 'Score'
}
def main():
    usage = """usage: %prog xquest_filename > out.csv
    Convert to proper csv.

    E.g:
    python %prog ours_apo.tsv > out.csv
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    xquest_filename = args[0]
    # raise NotImplementedError("This script is not yet implemented")
    xlink_set = pyxlinks.XlinksSet(xquest_filename)
    # xlink_set.fieldnames = [replace_dict.get(item,item) for item in xlink_set.fieldnames]

    out = [
        ['Protein1', 'Protein2', 'PepSeq1', 'PepSeq2', 'LinkPos1', 'LinkPos2', 'PepPos1', 'PepPos2', 'Score']
    ]
    for xlink in xlink_set.data:
        Protein1 = pyxlinks.get_protein(xlink, 1)
        Protein2 = pyxlinks.get_protein(xlink, 2)
        PepSeq1, PepSeq2 = pyxlinks.get_pepts(xlink)

        score = pyxlinks.get_score(xlink)

        if pyxlinks.is_loop_link(xlink):
            LinkPos1, LinkPos2 = pyxlinks.get_rel_pos(xlink)[0]
            AbsPos1 = int(pyxlinks.get_AbsPos(xlink, 1))
            AbsPos2 = int(pyxlinks.get_AbsPos(xlink, 2))
            peppos1 = str(AbsPos1 - LinkPos1 + 1)
            out_items = [Protein1, '-', PepSeq1, '-', LinkPos1, LinkPos2, peppos1, '-', score]
            
        elif pyxlinks.is_mono_link(xlink):
            AbsPos1 = int(pyxlinks.get_AbsPos(xlink, 1))
            peppos1 = str(AbsPos1 - LinkPos1+1)
            out_items = [Protein1, '-', PepSeq1, '-', LinkPos1, '-', peppos1, '-', score]

        else:
            AbsPos1 = int(pyxlinks.get_AbsPos(xlink, 1))
            AbsPos2 = int(pyxlinks.get_AbsPos(xlink, 2))
            LinkPos1, LinkPos2 = [int(pos[0]) for pos in pyxlinks.get_rel_pos(xlink)]

            peppos1 = str(AbsPos1 - LinkPos1+1)
            peppos2 = str(AbsPos2 - LinkPos2+1)
            out_items = [Protein1, Protein2, PepSeq1, PepSeq2, LinkPos1, LinkPos2, peppos1, peppos2, score]

        out.append(out_items)

    for item in out:
        print(','.join(map(str, item)))
            
if __name__ == '__main__':
    main()
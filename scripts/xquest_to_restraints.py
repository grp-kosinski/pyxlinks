#!/usr/bin/python
import sys
import json
from pprint import pprint
from itertools import product
from operator import itemgetter
from optparse import OptionParser

from Bio import PDB

import pyxlinks


def get_children(particle):
    return particle.get('assembly_of')

def get_particles_by_x_name(particles_data, x_name):
    out_particles = []
    for particle in particles_data:
        if 'x_names' in particle:
            if x_name in particle['x_names']:
                out_particles.append(particle)

        children_particles = get_children(particle)
        if children_particles:
            for child_particle in children_particles:
                if 'x_names' in child_particle:
                    if x_name in particle['x_names']:
                        out_particles.append(child_particle)
                        child_particle['parent'] = particle

    return out_particles

def get_xlinked_partners(xlink, particles_data, x_prot_name, x_pos):
    xlinked_partners = []
    for particle in particles_data:
        if 'x_names' in particle:
            if x_prot_name in particle['x_names']:
                if 'chains' in particle:
                    for chain in particle['chains']:
                        if has_position(particle, chain, x_pos):
                            xlinked_partners.append({
                                    'xlink': xlink,
                                    'particle': particle,
                                    'chain': chain,
                                    'pos': x_pos
                                })


        children_particles = get_children(particle)
        if children_particles:
            for child_particle in children_particles:
                if 'x_names' in child_particle:
                    if x_prot_name in child_particle['x_names']:
                        child_particle['parent'] = particle
                        if 'chains' in child_particle:
                            for chain in child_particle['chains']:
                                if has_position(child_particle, chain, x_pos):
                                    xlinked_partners.append({
                                            'xlink': xlink,
                                            'particle': child_particle,
                                            'chain': chain,
                                            'pos': x_pos
                                        })



    return xlinked_partners


def get_particle_structure(particle):
    pdb_structure = particle.get('pdb_structure')
    if pdb_structure is None:
        if 'parent' in particle:
            pdb_structure = particle['parent'].get('pdb_structure')    

    return pdb_structure

def has_position(particle, chain, pos):
    pdb_structure = get_particle_structure(particle)

    if pdb_structure is not None:
        for pdb_chain in pdb_structure.get_chains():
            pdb_chain_id = pdb_chain.get_id()
            if pdb_chain_id == chain:
                if int(pos) in pdb_chain:
                    return True
    
    return False

def add_pdb_structure_objects_to_particles(particles_data):
    #parse all pdb files and store structure objects in particles definitions
    for particle in particles_data:
        if 'pdb_filename' in particle:
            parser = PDB.PDBParser(QUIET=True)
            structure = parser.get_structure(particle['id'], particle['pdb_filename'])
            particle['pdb_structure'] = structure

def is_intra_xlink(xlink):
    return xlink['Protein1'] == xlink['Protein2']


def are_partners_same(partner1, partner2):
    id1, id2 = [get_id_of_particle_with_pdb(p) for p in (partner1, partner2)]
    return (id1 == id2) and (partner1['chain'] == partner2['chain']) and (partner1['pos'] == partner2['pos'])

def is_intra_xlink_feasible(partner1, partner2):
    is_feasible = False

    if partner1['pos'] != partner2['pos']:
        pdb_structure = get_particle_structure(partner1['particle'])
        chain1_id = partner1['chain']
        chain2_id = partner2['chain']

        chain1 = None
        chain2 = None
        if pdb_structure is not None:
            for pdb_chain in pdb_structure.get_chains():
                pdb_chain_id = pdb_chain.get_id()
                if pdb_chain_id == chain1_id:
                    chain1 = pdb_chain
                if pdb_chain_id == chain2_id:
                    chain2 = pdb_chain

        if chain1 is not None and chain2 is not None:
            residue1 = chain1[int(partner1['pos'])]
            residue2 = chain2[int(partner2['pos'])]
            dist = residue1['CA'] - residue2['CA']


            xlinker_name = partner1['xlink']['xlinker']
            xlinker_width = get_xlinker_width(xlinker_name)

            if dist < xlinker_width + 2:
                is_feasible = True

    return is_feasible



def get_restraints_present_in_particles(unique_xlinks_set, particles_data):
    restraints = []
    for xlink in unique_xlinks_set.data:

        prot1_restrained_particles = get_xlinked_partners(xlink,
                                                            particles_data,
                                                            x_prot_name=xlink['Protein1'],
                                                            x_pos=xlink['AbsPos1'])

        prot2_restrained_particles = get_xlinked_partners(xlink,
                                                            particles_data,
                                                            x_prot_name=xlink['Protein2'],
                                                            x_pos=xlink['AbsPos2'])



        restraint_pairs = list(product(prot1_restrained_particles, prot2_restrained_particles))
        # print restraint_pairs

        #ignore intra xlinks that can xlink resi within particle
        #because we don't know if it was within a molecule or to another copy:
        #TODO:  put out such xlinks on the reserve list
        #reserve list could be used later if it turns out from initial models that the xlink is feasible for both intra and inter 
        if is_intra_xlink(xlink):

            #check if intra xlink is feasible within subunits
            #if yes don't add inter-subunit (to other copies in case there are multiple copies) restraint

            for partner1, partner2 in restraint_pairs:
                is_feasible = False
                if partner1['particle']['id'] == partner2['particle']['id']:
                    is_feasible = is_intra_xlink_feasible(partner1, partner2)

                    if not is_feasible and not are_partners_same(partner1, partner2):
                        print_restraints([[partner1, partner2]])
                        restraints.append((partner1, partner2))
                    # break
        else:
            restraints.extend(restraint_pairs)


    return restraints

def get_id_of_particle_with_pdb(partner):
    particle = partner['particle']
    if 'pdb_structure' in particle:
        id = particle['id']
    elif 'parent' in particle:
        id = particle['parent']['id']
    else:
        id = None

    return id

def get_xlinker_obj_from_name(xlinker_name):
    return getattr(pyxlinks, xlinker_name)()

def get_xlinker_width(xlinker_name):
    xlinker = get_xlinker_obj_from_name(xlinker_name)
    width = xlinker.get_len_with_sidechains()

    return width

# def print_restraints(restraints):
#     for partner1, partner2 in restraints:
#         print partner1['particle']['id'], partner1['chain'], partner1['pos'], '-', partner2['particle']['id'], partner2['chain'], partner2['pos'], '-', partner1['xlink']['xlinker']

def print_restraints(restraints):
    for partner1, partner2 in restraints:
        id1, id2 = [get_id_of_particle_with_pdb(p) for p in (partner1, partner2)]

        if id1 != id2: #don't include intra-xlinks within same particle_with_pdb
            xlinker_name = partner1['xlink']['xlinker']
            restraint_max = get_xlinker_width(xlinker_name)

            print(' '.join([id1, partner1['chain'], partner1['pos'], id2, partner2['chain'], partner2['pos'], str(restraint_max)]))

def print_restraints_for_EMageFit(restraints):
    out_data = []


    for partner1, partner2 in restraints:
        id1, id2 = [get_id_of_particle_with_pdb(p) for p in (partner1, partner2)]

        if id1 != id2: #don't include intra-xlinks within itself
            xlinker_name = partner1['xlink']['xlinker']
            restraint_max = get_xlinker_width(xlinker_name)

            weight = 100
            stddev = 2
            out_data.append(
                [id1, partner1['chain'], int(partner1['pos']), id2, partner2['chain'], int(partner2['pos']), restraint_max, weight, stddev]
                )
            # print ' '.join([id1, partner1['chain'], partner1['pos'], id2, partner2['chain'], partner2['pos']])

    print(json.dumps(out_data))

def print_restraints_for_MultiFit(restraints):
    # out_data = []

    xlinked_pairs = set([])
    print('|residue-xlink|')
    for partner1, partner2 in restraints:
        id1, id2 = [get_id_of_particle_with_pdb(p) for p in (partner1, partner2)]
        xlinked_pairs.add('%^&'.join(sorted([id1, id2])))
        if id1 != id2: #don't include intra-xlinks within itself
            xlinker_name = partner1['xlink']['xlinker']
            restraint_max = get_xlinker_width(xlinker_name)

            # weight = 100
            # stddev = 2

            print('|1|{p1}|{resi1}|{p2}|{resi2}|{restraint_max}|'.format(p1=id1,
                                                                        resi1=partner1['pos'],
                                                                        p2=id2,
                                                                        resi2=partner1['pos'],
                                                                        restraint_max=restraint_max))
            # out_data.append(
            #     [id1, partner1['chain'], int(partner1['pos']), id2, partner2['chain'], int(partner2['pos']), restraint_max, weight, stddev]
            #     )

    print('|ev-pairs|')
    for ids in xlinked_pairs:
        print('|{0}|{1}|'.format(*ids.split('%^&')))

def _get_unique_residue_pair_marker(restraint):

    # before = []
    # for p in restraint:
    #     before.extend([get_id_of_particle_with_pdb(p), p['chain'], p['pos']])

    # print '_'.join(before)

    s = sorted(restraint, key=lambda p: get_id_of_particle_with_pdb(p))
    s = sorted(restraint, key=itemgetter('chain'))
    s = sorted(restraint, key=itemgetter('pos'))



    out = []
    for p in s:
        out.extend([get_id_of_particle_with_pdb(p), p['chain'], p['pos']])

    # print '>', '_'.join(out)

    return '_'.join(out)

def keep_with_shorter_xlinker(restraints):
    #first pass, create groups
    temp = []
    groups = {}
    for restraint in restraints:
        marker = _get_unique_residue_pair_marker(restraint)
        if marker in groups:
            groups[marker].append(restraint)
        else:
            groups[marker] = [restraint]

        temp.append((restraint, groups[marker]))

    #just print groups for testing
    # both_xlinkers = 0
    # for g in groups:
    #     if len(groups[g]) == 2:
    #         both_xlinkers +=1
    #         # print g
    #         print "----------------"
    #         for r in groups[g]:
    #             dupa = []
    #             for p in r:
    #                 dupa.extend([get_id_of_particle_with_pdb(p), p['chain'], p['pos'], p['xlink']['xlinker']])
    #             print " ".join(dupa)            
    # print both_xlinkers

    out = []
    for restraint, group in temp:
        if len(group) == 2:
            same_pairs = group
            shortest = min(same_pairs, key=lambda x: get_xlinker_width(x[0]['xlink']['xlinker']))
            if restraint is shortest:
                out.append(shortest)

        else:
            out.append(restraint)

    return out

def main():
    usage = "usage: %prog [options] xquest_file_list_filename particles_data_filename"
    parser = OptionParser(usage=usage)

    parser.add_option("-e", '--emagefit', action="store_true", dest="emagefit", help='Print in format for emagefit')
    parser.add_option("-m", '--multifit', action="store_true", dest="multifit", help='Print in format for MultiFit')
    (options, args) = parser.parse_args()


    xquest_file_list_filename = args[0] #list of filenames and xlinker names
    particles_data_filename = args[1]

    all_xlinks_set = pyxlinks.xquest_file_list_to_merged_xlink_set(xquest_file_list_filename)

    unique_xlinks_set = all_xlinks_set.get_unique_set_for_dist_restraints()



    with open(particles_data_filename) as f:
        particles_data = json.load(f)

    add_pdb_structure_objects_to_particles(particles_data)

    restraints = get_restraints_present_in_particles(unique_xlinks_set, particles_data)

    restraints = keep_with_shorter_xlinker(restraints)

    if options.emagefit:
        print_restraints_for_EMageFit(restraints)
    elif options.multifit:
        print_restraints_for_MultiFit(restraints)
    else:
        print_restraints(restraints)
if __name__ == '__main__':
    main()

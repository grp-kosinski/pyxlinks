source ~/bin/activate_python2.7_environment.sh
source /struct/cmueller/kosinski/software/ENV2.7/bin/activate

python ../../expected_from_pdb.py -t 30 -m 1 -d PolI_data_cfg.json 4C3I.pdb out_test_data/out.csv

python /struct/cmueller/kosinski/devel/pyxlinks/scripts/make_stats.py -o out_test_data out_test_data/out.csv

java -Xmx1024m -cp /struct/cmueller/kosinski/software/xwalk/bin/ Xwalk -infile 4C3I.pdb -aa1 LYS -aa2 LYS > 4C3I.xwalk.out
python ../../expected_from_pdb.py -t 30 -m 1 -d PolI_data_cfg.json 4C3I.pdb -x 4C3I.xwalk.out out_test_data/out_xwalk_dists.csv
python /struct/cmueller/kosinski/devel/pyxlinks/scripts/make_stats.py -o out_test_data out_test_data/out_xwalk_dists.csv



###          training          ###

cd PolI
python ../../../expected_from_pdb.py -t 30 -m 1 -d PolI_data_cfg.json 4C3I.pdb out_test_data/expected_from_pdb.out.csv
python ../../../expected_from_pdb.py -t 30 --min_rasa 0.1 -m 1 -d PolI_data_cfg.json 4C3I.pdb out_test_data/expected_from_pdb.out.csv

python ../../../make_stats.py --min_ld_score 30. -o out_test_data out_test_data/expected_from_pdb.out.csv
python ../../../make_stats.py --min_ld_score 30. --skip_intra --skip_inter -o out_test_data out_test_data/expected_from_pdb.out.csv

python ../../../train.py monolinks out_test_data/Observed.csv out_test_data/Expected__not_observed.csv
python ../../../train.py xlinks out_test_data/Observed.csv out_test_data/Expected__not_observed.csv


#the final predictor was made with cmds:
python ../../../expected_from_pdb.py -t 30 -m 1 -d PolI_data_cfg.json 4C3I.pdb out_test_data/expected_from_pdb.out.csv
python ../../../make_stats.py --min_ld_score 30. --skip_intra --skip_inter -o out_test_data out_test_data/expected_from_pdb.out.csv
python ../../../train.py monolinks out_test_data/Observed.csv out_test_data/Expected__not_observed.csv


1. expected_from_pdb.py 
- adds xlinks and monolinks expected from PDB
- adds columns with features
2. make_stats.py
- makes Observed.csv and Expected__not_observed.csv
- makes stats of features as boxplots (monolinks and xlinks combined)
- makes lists: observed_satisfied.csv, observed_not_satisfied.csv, observable_not_observed.csv (observable - accoring criteria in is_observable)
- Pipeline:
    * keep with 5 < length < 50
    * keep with ld-Score > 30
    * [optionally] skip intra
    * uniquify by (prot1, pept1), (prot2, pept2) (so modification resi doens't matter)
3. train.py
- performs training
- returns observed_used.csv and expected_used.csv - rows used for classification (training + testing)
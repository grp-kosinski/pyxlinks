#Alex data downloading uniprot seqs based on ids
python-2.7 ../make_input_for_crosslinkviewer.py   alex_data_file_list.Sample5.txt Sample_5.json Sample_5_all.csv

#Alex data with sequences provided in data
python-2.7 ../make_input_for_crosslinkviewer.py -s Alex_data/Sample_5.fasta  alex_data_file_list.Sample5.txt Sample_5.json Sample_5_all.csv

#Elongator data  downloading uniprot seqs based on ids and using name to uniprot id map file
python-2.7 ../make_input_for_crosslinkviewer.py -n xquest_names_to_ids.txt xquest_file_list.txt elongator_all.json elongator_all.csv
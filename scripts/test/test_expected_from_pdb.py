import os
import unittest
import re
import tempfile

import Bio
import Bio.PDB

import pyxlinks


class TestFunctions(unittest.TestCase):
    def test_cleave(self):
        seq = 'SGKRKPNIILDEDDTNDGIER'
        # pyxlinks.cleave

class XlinkAnalyzerTest(unittest.TestCase):
    def setUp(self):
        self.example = 'PolI' #used for output files

        self.data_config_filename = 'PolI/PolI_data_cfg.json'

        self.components = ['A190', 'A135', 'ABC10beta', 'ABC10alpha', 'AC40', 'AC19', 'A12', 'A34.5', 'A49', 'ABC27', 'A14', 'A43', 'ABC23', 'ABC14.5']

        self.seq = 'MSNIVGIEYNRVTNTTSTDFPGFSKDAENEWNVEKFKKDFEVNISSLDAREANFDLINID\
TSIANAFRRIMISEVPSVAAEYVYFFNNTSVIQDEVLAHRIGLVPLKVDPDMLTWVDSNL\
PDDEKFTDENTIVLSLNVKCTRNPDAPKGSTDPKELYNNAHVYARDLKFEPQGRQSTTFA\
DCPVVPADPDILLAKLRPGQEISLKAHCILGIGGDHAKFSPVSTASYRLLPQINILQPIK\
GESARRFQKCFPPGVIGIDEGSDEAYVKDARKDTVSREVLRYEEFADKVKLGRVRNHFIF\
NVESAGAMTPEEIFFKSVRILKNKAEYLKNCPITQ'
        
        self.pdb_filename = 'PolI/4C3I.pdb'

        self.distances = [
            (('A',5),('A',70),19.500),
            (('A',1012),('B',1043),28.545)
        ]

    def test_init(self):
        xlinkAnalyzer = pyxlinks.XlinkAnalyzer(self.data_config_filename)

        self.assertEqual(xlinkAnalyzer.cfg.components, self.components)
        

    def test_make_xlinked_peptides_for_all_seqs(self):
        xlinkAnalyzer = pyxlinks.XlinkAnalyzer(self.data_config_filename)

        missed_cleavages = 1
        min_pept_length = None
        max_pept_length = None
        xlinkAnalyzer.make_xlinked_peptides_for_all_seqs(missed_cleavages, min_pept_length, max_pept_length)


    def test_make_xlinked_peptides(self):
        peptides = pyxlinks.make_xlinked_peptides(self.seq, missed_cleavages=1, min_pept_length=None, max_pept_length=None, rule='[KR](?=[^P])')

        # print self.seq
        # print peptides.ordered_list

        for pept in peptides.ordered_list:
            ref_pept = self.seq[pept['start_resi']-1:pept['start_resi']-1+len(pept['seq'])]
            self.assertEqual(ref_pept, pept['seq'])
            for mod_pos in pept['mod_pos']:
                self.assertEqual('K', pept['seq'][mod_pos])

    def test_make_xlinked_peptides_for_all_seqs(self):
        missed_cleavages = 1
        xlinkAnalyzer = pyxlinks.XlinkAnalyzer(self.data_config_filename)
        xlinkAnalyzer.make_xlinked_peptides_for_all_seqs(missed_cleavages=missed_cleavages, min_pept_length=None, max_pept_length=None)
        xlinkAnalyzer.load_xlinks()
        xlinkAnalyzer.test_theoretical_peptides()



        for comp, peptides in xlinkAnalyzer.theoretical_peptides.items():
            full_seq = xlinkAnalyzer.cfg.get_seq(comp)
            for pept in peptides.ordered_list:
                test_peptide = pept['seq']
                missed_cleavages_found = len(list(re.finditer(xlinkAnalyzer.cleavage_rule, test_peptide))) - len(pept['mod_pos'])
                # print pept, missed_cleavages
                self.assertGreaterEqual(missed_cleavages, missed_cleavages_found)
                self.assertNotEqual(-1, full_seq.find(pept['seq']))

    def test_gen_xlinks_possible_in_structure(self):
        '''Test the ContactMatrix generation and getting the list of contacts of crosslikable pairs'''
        pdb_structure = Bio.PDB.PDBParser().get_structure(self.pdb_filename, self.pdb_filename)
        missed_cleavages = 1
        xlink_possible_fn = lambda r1, r2: pyxlinks.is_pair_crosslinkable(r1, r2, biodssp=None, acc_thresh=None)
        xlinkAnalyzer = pyxlinks.XlinkAnalyzer(self.data_config_filename)

        xlinkAnalyzer.gen_xlinks_possible_in_structure(pdb_structure, xlink_len_threshold=30, possible_fn=xlink_possible_fn)


        out_lines = ['open ' + self.pdb_filename]
        for xlink in xlinkAnalyzer.possible_xlinks:
            out_lines.append('distance :{resi1}.{chain1}@CA :{resi2}.{chain2}@CA'.format(chain1=xlink[0][0], resi1=xlink[0][1], chain2=xlink[1][0], resi2=xlink[1][1]))


        log_filename = os.path.join(tempfile.gettempdir(), '{0}_{1}.cmd'.format(self.example, self._testMethodName))
        with open(log_filename, 'w') as f:
            f.write('\n'.join(out_lines))

        for dist in self.distances:
            self.assertAlmostEqual(xlinkAnalyzer.contactMatrix.matrix[dist[0][0], dist[0][1]][dist[1][0], dist[1][1]], dist[2], places=3)

class XlinkAnalyzerTest_Pol3(XlinkAnalyzerTest):
    def setUp(self):
        self.example = 'PolIII' #used for output files

        self.data_config_filename = 'PolIII/data_cfg.json'

        self.components = [
            "C160",
            "C128",

            "ABC10beta",
            "ABC10alpha",
            "AC40",
            "AC19",

            "C11",
            "C53",
            "C37",

            "C82",
            "C34",
            "C31",
            "ABC27",


            "C17",
            "C25",
            "ABC23",
            "ABC14.5"
        ]

#         self.seq = 'MKEVVVSETPKRIKGLEFSALSAADIVAQSEVEVSTRDLFDLEKDRAPKANGALDPKMGV\
# SSSSLECATCHGNLASCHGHFGHLKLALPVFHIGYFKATIQILQGICKNCSAILLSETDK\
# RQFLHELRRPGVDNLRRMGILKKILDQCKKQRRCLHCGALNGVVKKAAAGAGSAALKIIH\
# DTFRWVGKKSAPEKDIWVGEWKEVLAHNPELERYVKRCMDDLNPLKTLNLFKQIKSADCE\
# LLGIDATVPSGRPETYIWRYLPAPPVCIRPSVMMQDSPASNEDDLTVKLTEIVWTSSLIK\
# AGLDKGISINNMMEHWDYLQLTVAMYINSDSVNPAMLPGSSNGGGKVKPIRGFCQRLKGK\
# QGRFRGNLSGKRVDFSGRTVISPDPNLSIDEVAVPDRVAKVLTYPEKVTRYNRHKLQELI\
# VNGPNVHPGANYLLKRNEDARRNLRYGDRMKLAKNLQIGDVVERHLEDGDVVLFNRQPSL\
# HRLSILSHYAKIRPWRTFRLNECVCTPYNADFDGDEMNLHVPQTEEARAEAINLMGVKNN\
# LLTPKSGEPIIAATQDFITGSYLISHKDSFYDRATLTQLLSMMSDGIEHFDIPPPAIMKP\
# YYLWTGKQVFSLLIKPNHNSPVVINLDAKNKVFVPPKSKSLPNEMSQNDGFVIIRGSQIL\
# SGVMDKSVLGDGKKHSVFYTILRDYGPQEAANAMNRMAKLCARFLGNRGFSIGINDVTPA\
# DDLKQKKEELVEIAYHKCDELITLFNKGELETQPGCNEEQTLEAKIGGLLSKVREEVGDV\
# CINELDNWNAPLIMATCGSKGSTLNVSQMVAVVGQQIISGNRVPDGFQDRSLPHFPKNSK\
# TPQSKGFVRNSFFSGLSPPEFLFHAISGREGLVDTAVKTAETGYMSRRLMKSLEDLSCQY\
# DNTVRTSANGIVQFTYGGDGLDPLEMEGNAQPVNFNRSWDHAYNITFNNQDKGLLPYAIM\
# ETANEILGPLEERLVRYDNSGCLVKREDLNKAEYVDQYDAERDFYHSLREYINGKATALA\
# NLRKSRGMLGLLEPPAKELQGIDPDETVPDNVKTSVSQLYRISEKSVRKFLEIALFKYRK\
# ARLEPGTAIGAIGAQSIGEPGTQMTLKTFHFAGVASMNVTLGVPRIKEIINASKVISTPI\
# INAVLVNDNDERAARVVKGRVEKTLLSDVAFYVQDVYKDNLSFIQVRIDLGTIDKLQLEL\
# TIEDIAVAITRASKLKIQASDVNIIGKDRIAINVFPEGYKAKSISTSAKEPSENDVFYRM\
# QQLRRALPDVVVKGLPDISRAVINIRDDGKRELLVEGYGLRDVMCTDGVIGSRTTTNHVL\
# EVFSVLGIEAARYSIIREINYTMSNHGMSVDPRHIQLLGDVMTYKGEVLGITRFGLSKMR\
# DSVLQLASFEKTTDHLFDAAFYMKKDAVEGVSECIILGQTMSIGTGSFKVVKGTNISEKD\
# LVPKRCLFESLSNEAALKAN'

        self.seq = 'MSSNKGNGRLPSLKDSSSNGGGSAKPSLKFKPKAVARKSKEEREAAASKVKLEEESKRGN\
DKKHFNNKNKRVTGAGGQQRRMAKYLNNTHVISSGPLAAGNFVSEKGDLRRGFIKSEGSG\
SSLVQKGLETIDNGAESSENEAEDDDNEGVASKSKKKFNMGKEFEARNLIEDEDDGESEK\
SSDVDMDDEEWRSKRIEQLFPVRPVRVRHEDVETVKREIQEALSEKPTREPTPSVKTEPV\
GTGLQSYLEERERQVNEKLADLGLEKEFQSVDGKEAAAELELLNADHQHILRKLKKMNNK\
PERFMVFQLPTRLPAFERPAVKEEKEDMETQASDPSKKKKNIKKKDTKDALSTRELAGKV\
GSIRVHKSGKLSVKIGNVVMDIGKGAETTFLQDVIALSIADDASSAELLGRVDGKIVVTP\
QI'

        self.pdb_filename = 'PolIII/sol0.pdb'



        self.distances = [
            (('A',2),('A',11),14.409),
            (('O',520),('P',62),16.830)
        ]

if __name__ == '__main__':
    unittest.main(verbosity=2)
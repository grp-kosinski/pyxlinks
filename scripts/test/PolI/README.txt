source ~/bin/activate_python2.7_environment.sh

python-2.7 ../../expected_from_pdb.py -t 30 -m 1 -d PolI_data_cfg.json 4C3I.pdb out_test_data/out.csv

python-2.7 /struct/cmueller/kosinski/devel/pyxlinks/scripts/make_stats.py -o out_test_data out_test_data/out.csv

java -Xmx1024m -cp /struct/cmueller/kosinski/software/xwalk/bin/ Xwalk -infile 4C3I.pdb -aa1 LYS -aa2 LYS > 4C3I.xwalk.out
python-2.7 ../../expected_from_pdb.py -t 30 -m 1 -d PolI_data_cfg.json 4C3I.pdb -x 4C3I.xwalk.out out_test_data/out_xwalk_dists.csv
python-2.7 /struct/cmueller/kosinski/devel/pyxlinks/scripts/make_stats.py -o out_test_data out_test_data/out_xwalk_dists.csv
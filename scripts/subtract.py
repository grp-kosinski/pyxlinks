#!/usr/bin/python
from optparse import OptionParser

import pyxlinks

def main():
    usage = """usage: %prog [options] from_xquest_filename second_xquest_filename > out.csv
    Remove xlinks present in second_xquest_filename from from_xquest_filename
    E.g:
    python subtract.py ours_apo.csv ours_DNA.csv > out.csv
    """
    parser = OptionParser(usage=usage)

    parser.add_option("--no_fieldnames", action="store_true", dest="no_fieldnames", default=False,
                      help="Do not print fieldnames [default: %default]")

    (options, args) = parser.parse_args()

    from_xquest_filename = args[0]
    second_xquest_filename = args[1]


    from_xlink_set = pyxlinks.XlinksSet(from_xquest_filename)
    second_xquest_set = pyxlinks.XlinksSet(second_xquest_filename)
    
    out = from_xlink_set.get_difference(second_xquest_set, same_xlink_marker=pyxlinks._xlink2unique_marker_by_prots_resi)
    
    out.print_out()

if __name__ == '__main__':
    main()
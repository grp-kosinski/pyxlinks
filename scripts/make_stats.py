#!/usr/bin/python
import sys
import os
import csv
from optparse import OptionParser
from operator import itemgetter
import itertools
from collections import defaultdict

import matplotlib.pyplot as plt
import pylab

import pyxlinks

def grouprows(rows, fn):
    grouped = defaultdict(list)
    for row in rows:
        key = fn(row)
        if key is not None:
            grouped[key].append(row)

    return grouped

def expected_not_observed_vs_observed(row):
    if row['Observed'] == 'No' and row['Expected'] == 'Yes'  and row['FullyCleaved1'] == 'Yes' and row['FullyCleaved2'] in ('Yes', 'n/a'):
        return 'Expected, not observed'
    elif row['Observed'] == 'Yes':
        return 'Observed'

def make_boxplot(grouped, outfilename, fieldname=None, fn=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    if fieldname is not None:
        fn = lambda x: x[fieldname]

    data = []
    xticks = []
    for_legend = defaultdict(list)
    for k in grouped:
        xticks.append(k)
        group_data = []
        for row in grouped[k]:
            val = fn(row)
            if isinstance(val, float):
                group_data.append(val)

        data.append(group_data)
        for_legend[k].append([len(group_data)])

    ax.boxplot(data, sym='.')
    pylab.xticks([1, 2], xticks)
    
    y = 0.080
    for data_name, legend_items in for_legend.items():
        text = '{0}: {1} xlinks'.format(data_name, legend_items[0])
        plt.figtext(0.515, y, text)
        y = y - 0.040
    plt.savefig(outfilename)

def is_in_xquest_len_range(row, min_len, max_len):
    '''min_len, max_len exclusive (< not <=)'''
    if isinstance(row['Length1'], float) and not (min_len < row['Length1'] < max_len):
        return False

    if isinstance(row['Length2'], float) and not (min_len < row['Length2'] < max_len):
        return False

    return True

def is_ld_score_higher_than(row, threshold):
    if row['Observed'] == 'Yes':
        if float(row['ld-Score']) >= threshold:
            return True
        else:
            return False

    return True

# def get_worse_rASA(row):

#     out = []
#     for rASA in (row['rASA1'], row['rASA2']):
#         if isinstance(rASA, float):
#             out.append(rASA)
#     if len(out) > 0:
#         return min(out)

def get_higher_hub_score(row):

    out = []
    for score in (row['Hub_score1'], row['Hub_score2']):
        if isinstance(score, float):
            out.append(score)
    if len(out) > 0:
        return max(out)

def save_csv_for_control(grouped, fieldnames, outdir=None):
    if outdir is None:
        outdir = os.getcwd()

    for k in grouped:
        filename = k.replace(',', '_').replace(' ', '_') + '.csv'
        write_csv(os.path.join(outdir, filename), grouped[k], fieldnames)

def write_csv(filename, rows, fieldnames):
    with open(filename, 'wb') as f:
        wr = csv.DictWriter(f, fieldnames, quoting=csv.QUOTE_NONNUMERIC)        
        wr.writeheader()
        wr.writerows(rows)

def is_observed(row):
    return row['Observed'] == 'Yes'

def is_not_observed(row):
    return row['Observed'] == 'No'

def is_expected(row):
    return row['Expected'] == 'Yes'

def is_not_expected(row):
    return row['Expected'] == 'No'

def is_observed_satisfied(row):
    return row['Observed'] == 'Yes' and row['Expected'] == 'Yes'

def is_observed_not_satisfied(row):
    return row['Observed'] == 'Yes' and row['Expected'] == 'No'

def is_observable(row):
    return row['LengthTotal'] < 25 and row['Hub_score_total'] < 40 and is_in_xquest_len_range(row, 5, 50) and row['HydrophobicityTotal'] < -0.2


# def is_mono_link(row):
#     return None in (row['xProtein1'], row['xProtein2'])

def _xlink2unique_marker_by_prots_resi_FOR_NON_OBSERVED(single_xlink_data):
    x = single_xlink_data


    comp1 = pyxlinks.get_protein(x, 1)
    comp2 = pyxlinks.get_protein(x, 2)

    resi1 = pyxlinks.get_AbsPos(x, 1)
    resi2 = pyxlinks.get_AbsPos(x, 2)

    prot_resi = [
        [comp1, str(resi1)],
        [comp2, str(resi2)]
    ]

    if comp1 == comp2:
        prot_resi.sort(key=lambda s: s[0] + s[1])
    else:
        prot_resi.sort(key=itemgetter(0))

    marker = '_'.join([prot_resi[0][0], prot_resi[0][1], prot_resi[1][0], prot_resi[1][1]])


    return marker

def print_some_stats(rows, fieldnames, ld_score_threshold=None, outdir=None):
    observed_satisfied = []
    observed_not_satisfied = []

    # unique_by_prot_resi = pyxlinks.get_unique(rows, idfun=pyxlinks._xlink2unique_marker_by_prots_resi)

    observable_not_satisfied = []

    for row in rows:
        if ld_score_threshold is not None and is_observed(row) and row['ld-Score'] < ld_score_threshold:
            continue

        if is_observed_satisfied(row):
            observed_satisfied.append(row)
        elif is_observed_not_satisfied(row):
            observed_not_satisfied.append(row)

    observed_satisfied = pyxlinks.get_unique(observed_satisfied, idfun=pyxlinks._xlink2unique_marker_by_prots_resi)
    observed_not_satisfied = pyxlinks.get_unique(observed_not_satisfied, idfun=pyxlinks._xlink2unique_marker_by_prots_resi)

    for row in rows:
        if is_expected(row) and is_observable(row) and is_not_observed(row):
            observable_not_satisfied.append(row)

    observable_not_satisfied = pyxlinks.get_unique(observable_not_satisfied, idfun=_xlink2unique_marker_by_prots_resi_FOR_NON_OBSERVED)

    print('Observed satisfied:', len(observed_satisfied))
    print('Observed but not satisfied:', len(observed_not_satisfied))
    print('Fraction:', float(len(observed_not_satisfied))/len(observed_satisfied)*100, '%')

    if outdir is None:
        outdir = os.getcwd()

    write_csv(os.path.join(outdir, 'observed_satisfied.csv'), observed_satisfied, fieldnames)
    write_csv(os.path.join(outdir, 'observed_not_satisfied.csv'), observed_not_satisfied, fieldnames)

    print('Expected but observable and not observed', len(observable_not_satisfied))
    write_csv(os.path.join(outdir, 'observable_not_observed.csv'), observable_not_satisfied, fieldnames)


def main():
    '''
    Pipeline:
    * keep with 5 < length < 50
    * keep with ld-Score > 30
    * [optionally] skip intra
    * uniquify by (prot1, pept1), (prot2, pept2) (so modification resi doens't matter)
    '''
    usage = "usage: %prog [options] csv_filename"
    parser = OptionParser(usage=usage)

    parser.add_option("-x", "--exclude", dest="exclude_pairs_filename",
                      help="optional pairs between which xlinks should be excluded", metavar="FILE")

    parser.add_option("-c", "--exclude_subcomplexes", dest="exclude_subcomplexes_filename",
                      help="optional subcomplexes within which xlinks should be excluded", metavar="FILE")

    parser.add_option("--skip_intra", action="store_true", dest="skip_intra", default=False,
                      help="skip intra xlinks")

    parser.add_option("--skip_inter", action="store_true", dest="skip_inter", default=False,
                      help="skip inter xlinks")

    parser.add_option("-l", "--min_ld_score", type="float", dest="min_ld_score", default=0.,
                      help="min_ld_score [default: %default]")

    parser.add_option("-o", "--outdir", dest="outdir",
                      help="output directory", metavar="DIR")    

    (options, args) = parser.parse_args()

    filename = args[0]

    exclude_pairs = set([])
    if options.exclude_pairs_filename:
        raise NotImplementedError


    if options.exclude_subcomplexes_filename:
        with open(options.exclude_subcomplexes_filename) as f:
            components = f.read().splitlines()
            for comp1, comp2 in itertools.combinations_with_replacement(components, 2):
                exclude_pairs.add(frozenset((comp1, comp2)))

    if options.outdir is None:
        outdir = os.getcwd()
    else:
        outdir = options.outdir


    with open(filename, 'rU') as csvfile: #'U' is necessary, otherwise sometimes crashes with _csv.Error: new-line character seen in unquoted field - do you need to open the file in universal-newline mode?
        dialect = csv.Sniffer().sniff(csvfile.readline(), ['\t', ','])
        csvfile.seek(0)

        reader = csv.DictReader(csvfile, dialect=dialect, quoting=csv.QUOTE_NONNUMERIC)

        rows = [row for row in reader if is_in_xquest_len_range(row, 5, 50)]

        rows = [row for row in rows if is_ld_score_higher_than(row, options.min_ld_score)]


        seen_dict = {}
        unique = []
        for row in rows:
            # prot1 = row['Protein1']
            # prot2 = row['Protein2']

            prot1 = pyxlinks.get_protein(row, 1)
            prot2 = pyxlinks.get_protein(row, 2)

            if options.skip_intra and prot1 == prot2:
                continue

            if options.skip_inter and pyxlinks.is_inter(row):
                continue

            if frozenset((prot1, prot2)) in exclude_pairs:
                continue

            # if pyxlinks.is_mono_link(row):
            #     continue

            pept1, pept2 = pyxlinks.get_pepts(row)


            # if (pept1, pept2) == (None, None):
            #     pepts = row['Theoretical'].split('-')

            #     if len(pepts) == 4: #normal xlinks
            #         pept1 = pepts[0]
            #         pept2 = pepts[1]
            #     elif len(pepts) == 2: #looplinks, monolinks:
            #         pept1 = pepts[0]
            #         pept2 = None

            # if (pept1, petp2) == (None, None):
            #     print pept1, pept2

            key = frozenset([(prot1, pept1), (prot2, pept2)])

            if key not in seen_dict:
                unique.append(row)
                seen_dict[key] = row
            else:
                row_prev = seen_dict[key]

                if '-' not in (row['ld-Score'], row_prev['ld-Score']) and (float(row['ld-Score']) > float(row_prev['ld-Score'])) or \
                     row_prev['Observed'] == 'No' and row['Observed'] == 'Yes':
                    idx = unique.index(row_prev)
                    unique[idx] = row
                    seen_dict[key] = row



        print(len(unique))
        rows = unique

        print_some_stats(rows, reader.fieldnames, outdir=outdir)

        grouped = grouprows(rows, fn=expected_not_observed_vs_observed)

        make_boxplot(grouped, os.path.join(outdir, 'HydrophobicityTotal.png'), fieldname='HydrophobicityTotal')
        make_boxplot(grouped, os.path.join(outdir, 'rt_Guo1986Total.png'), fieldname='rt_Guo1986Total')
        make_boxplot(grouped, os.path.join(outdir, 'rt_Meek1980Total.png'), fieldname='rt_Meek1980Total')
        make_boxplot(grouped, os.path.join(outdir, 'WorseAccessibiltity.png'), fn=pyxlinks.get_worse_rASA)
        make_boxplot(grouped, os.path.join(outdir, 'LengthTotal.png'), fieldname='LengthTotal')
        make_boxplot(grouped, os.path.join(outdir, 'hydrophobicTotal.png'), fieldname='hydrophobicTotal')
        make_boxplot(grouped, os.path.join(outdir, 'chargedTotal.png'), fieldname='chargedTotal')
        make_boxplot(grouped, os.path.join(outdir, 'hydrophilicTotal.png'), fieldname='hydrophilicTotal')
        make_boxplot(grouped, os.path.join(outdir, 'MassTotal.png'), fieldname='MassTotal')
        make_boxplot(grouped, os.path.join(outdir, 'Hub_score_higher.png'), fn=get_higher_hub_score)
        make_boxplot(grouped, os.path.join(outdir, 'Hub_score_total.png'), fieldname='Hub_score_total')
        make_boxplot(grouped, os.path.join(outdir, 'net_charge.png'), fieldname='net_charge')

        save_csv_for_control(grouped, reader.fieldnames, outdir=outdir)

if __name__ == '__main__':
    main()
#!/usr/bin/python
import sys
from itertools import combinations_with_replacement
from optparse import OptionParser

import pyxlinks

def main():
    if len(sys.argv) < 2:
        print('Usage: {0} xquest_file_list_filename'.format(sys.argv[0]))
        sys.exit()

    usage = "usage: %prog [options] xquest_file_list_filename|xquest_filename"
    parser = OptionParser(usage=usage)

    parser.add_option("-l", "--min_ld_score", type="float", dest="min_ld_score", default=None,
                      help="min_ld_score [default: %default]")

    parser.add_option("--xquest_file", action="store_true", dest="xquest_file", default=False,
                      help="Use xquest file, no xquest_file_list_filename [default: %default]")


    (options, args) = parser.parse_args()

    if options.xquest_file:
        xquest_filename = args[0]
        all_xlinks_set = pyxlinks.XlinksSet(filename=xquest_filename, add_extra_fields=False)
    else:
        xquest_file_list_filenames = args
        xquest_file_list_filename = args[0] #list of filenames and xlinker names
        all_xlinks_set = pyxlinks.xquest_file_list_to_merged_xlink_set(xquest_file_list_filename)

    if options.min_ld_score is not None:
        all_xlinks_set.filterByLdScore(options.min_ld_score)

    unique_xlinks_set = all_xlinks_set.get_unique_set_for_dist_restraints()


    # min_pept_len = 100000
    
    pept_lens = []
    for xlink in all_xlinks_set.data:
        pepts = pyxlinks.get_pepts(xlink)
        for pept in pepts:
            if pept is not None:
                pept_lens.append(len(pept))

    if pept_lens:
        print('Shortest peptide length:', min(pept_lens))
        print('Longest peptide length:', max(pept_lens))

    inter = []
    intra = []
    mono = []
    print('unique_by_prots_resi: ', len(all_xlinks_set.get_unique_by_prots_resi().data))
    print('unique_by_prots_resi_and_xlinker: ', len(all_xlinks_set.get_unique_by_prots_resi_and_xlinker().data))
    print('')

    for xlink in all_xlinks_set.get_unique_by_prots_resi().data:
        if pyxlinks.is_intra(xlink):
            intra.append(xlink)
        if pyxlinks.is_inter(xlink):
            inter.append(xlink)
        if pyxlinks.is_mono_link(xlink):
            mono.append(xlink)

    print('Number of unique inter:', len(inter))
    print('Number of unique intra (incl. loop links):', len(intra))
    print('Number of unique mono:', len(mono))


    print('--- All protein names: -------------------------------------')
    prot_names = unique_xlinks_set.get_protein_names()
    print('\n'.join(prot_names))

    print('--- Inter/intra for protein: -------------------------------------')
    for name in prot_names:
        print(name, len(unique_xlinks_set.get_inter_intra_for_prot(name).data))

    print('--- Xlinked pairs: -------------------------------------')
    for prot1, prot2 in combinations_with_replacement(prot_names, 2):
        pairs = {}
        print(prot1, prot2+':')
        # print unique_xlinks_set.get_for_prot_pair(prot1, prot2).print_xlinks()

        xlinks = unique_xlinks_set.get_for_prot_pair(prot1, prot2).data

        if prot1 != prot2:
            for xlink in xlinks:
                absPos1 = pyxlinks.get_AbsPos(xlink, 1)
                absPos2 = pyxlinks.get_AbsPos(xlink, 2)
                if absPos1 == 'n/a' and absPos2 == 'n/a':
                    print('Crosslink with both positions n/a !')
                    continue

                pos = {xlink['Protein1']: absPos1, xlink['Protein2']: absPos2}
                # print pos[prot1], pos[prot2], xlink['xlinker']
                key = '_'.join([pos[prot1], pos[prot2]])
                if key not in pairs:
                    pairs[key] = [xlink]
                else:
                    pairs[key].append(xlink)

        else:
            for xlink in xlinks:
                absPos1 = pyxlinks.get_AbsPos(xlink, 1)
                absPos2 = pyxlinks.get_AbsPos(xlink, 2)
                if absPos1 == 'n/a' and absPos2 == 'n/a':
                    print('Crosslink with both positions n/a !')
                    continue

                if not pyxlinks.is_mono_link(xlink):
                    key = '_'.join(sorted([absPos1, absPos2]))

                    if key not in pairs:
                        pairs[key] = [xlink]
                    else:
                        pairs[key].append(xlink)


        # for item in pairs.iteritems():
        #     print item
        #     print item[0]

        sorted_x = sorted(iter(pairs.items()), key=lambda x: int(x[0].split('_')[0]))
        for key, pair_xlinks in sorted_x:
            # first_xlink = pair_xlinks[0] #all pair_xlinks in this set have the same numbers
            print(' '.join(key.split('_')), ', '.join([str(x.get('xlinker')) for x in pair_xlinks]))

        if prot1 == prot2:
            print(prot1, prot2, '"Dimeric" xlinks:')
            for key, pair_xlinks in sorted_x:
                # print key, pair_xlinks
                xlinkers = []
                for x in pair_xlinks:
                    if x['Protein1'] == x['Protein2'] and x['AbsPos1'] == x['AbsPos2']:
                        xlinkers.append(x['xlinker'])
                if len(xlinkers) > 0:
                    print(' '.join(key.split('_')), ', '.join(xlinkers))

    print('--- Mono links: -------------------------------------')
    for name in prot_names:
        print(name)
        monolinks = unique_xlinks_set.get_mono_links_for_prot(name).data
        grouped_by_pos = {}
        for xlink in monolinks:
            pos = pyxlinks.get_mono_link_pos(xlink)
            if pos == 'n/a':
                print('Crosslink with both positions n/a !')
                continue


            if pos not in grouped_by_pos:
                grouped_by_pos[pos] = [xlink]
            else:
                grouped_by_pos[pos].append(xlink)
        sorted_x = sorted(iter(grouped_by_pos.items()), key=lambda x: int(x[0]))
        # sorted_x = sorted(monolinks, key=lambda x: int(pyxlinks.get_mono_link_pos(x)))
        for key, mono_xlinks in sorted_x:
            print(key, ', '.join([x['xlinker'] for x in mono_xlinks]))

if __name__ == '__main__':
    main()
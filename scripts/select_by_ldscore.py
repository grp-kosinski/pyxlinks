#!/usr/bin/python
from optparse import OptionParser

import pyxlinks


def main():
    usage = """usage: %prog [options] xquest_filename min_ld_score > outfile
    E.g:
    Generate xlink file with xlinks with ld-score at least min_ld_score
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    xquest_filename = args[0]
    ld_score = float(args[1])

    xlink_set = pyxlinks.XlinksSet(xquest_filename)
    xlink_set.filterByLdScore(ld_score)
    xlink_set.print_out()


if __name__ == '__main__':
    main()
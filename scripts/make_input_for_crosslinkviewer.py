#!/usr/bin/python
import re
import json
from optparse import OptionParser
import urllib.request, urllib.parse, urllib.error
from io import StringIO

from cogent.parse.fasta import MinimalFastaParser

import pyxlinks

def get_seq_data(fasta_filehandle):
    seq_data = []

    for name, seq in MinimalFastaParser(fasta_filehandle):
        m = re.match('(?:.*\|){0,1}(.*)\|(.*?)\s', name)
        
        if m is not None and len(m.groups()) == 2:
            # seq_data.append([m.group(1), m.group(2), seq])
            id_name = m.group(1)
            short_name = m.group(2)
        else:
            print('Had problems parsing uniprot ids from sequence "{0}"'.format(name))
        
        seq_data.append([id_name, short_name, seq])


    return seq_data

def get_names_to_ids(names_to_ids_filename):
    out = {}
    with open(names_to_ids_filename) as f:
        for line in f.read().splitlines():
            name, db_id = line.split()
            out[name] = db_id

    return out

def main():
    usage = "usage: %prog [options] xquest_file_list_file out_seq_data_json_filename out_xlink_data_csv_filename"
    parser = OptionParser(usage=usage)
    parser.add_option("-s", "--file", dest="seqs_in_fasta",
                      help="optional FILE with sequences in fasta format with uniprot deflines", metavar="FILE")

    parser.add_option("-n", "--names_to_ids", dest="names_to_ids",
                      help="optional FILE with protein names in xquest files mapped to uniprot ids", metavar="FILE")

    parser.add_option("-l", "--ld_score_min", dest="ld_score_min",
                      help="minimal ld-score")

    (options, args) = parser.parse_args()


    xquest_file_list_filename = args[0] #list of filenames and xlinker names
    all_xlinks_set = pyxlinks.xquest_file_list_to_merged_xlink_set(xquest_file_list_filename)


    if options.names_to_ids:
        names_to_ids = get_names_to_ids(options.names_to_ids)
    else:
        names_to_ids = None

    if options.seqs_in_fasta:
        seq_fasta_filename = options.seqs_in_fasta
        with open(seq_fasta_filename) as f:
            seq_data = get_seq_data(f)
    else:
        names = all_xlinks_set.get_protein_names()
        if names_to_ids:
            uniprot_ids = [names_to_ids[name] for name in names]
        else:
            uniprot_ids = [name.split('|')[1] for name in names]

        url = 'http://www.ebi.ac.uk/Tools/dbfetch/dbfetch/uniprotkb/{0}/fasta'.format(','.join(uniprot_ids))


        response = urllib.request.urlopen(url)
        page = response.read()

        seq_data = get_seq_data(StringIO(page))

    out_seq_data_json_filename = args[1]
    out_xlink_data_csv_filename = args[2]




    # with open(out_seq_data_json_filename, 'w') as f:
    #     json.dump(seq_data, f)



    with open(out_xlink_data_csv_filename, 'w') as f:
        if options.ld_score_min:
            all_xlinks_set.filterByLdScore(float(options.ld_score_min))
            filtered_only = True
        else:
            filtered_only = False

        f.write(all_xlinks_set.print_for_interaction_viewer(names_to_ids, filtered_only))



if __name__ == '__main__':
    main()
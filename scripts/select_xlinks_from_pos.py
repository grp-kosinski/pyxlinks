#!/usr/bin/python
from optparse import OptionParser

import pyxlinks

def main():
    usage = """usage: %prog [options] prot resi xquest_filenames
    E.g:
    python select.py "sp|P04051|RPC1_YEAST" 1242 ours_apo.csv ours_DNA.csv
    """
    parser = OptionParser(usage=usage)

    parser.add_option("--no_fieldnames", action="store_true", dest="no_fieldnames", default=False,
                      help="Do not print fieldnames [default: %default]")

    (options, args) = parser.parse_args()

    prot = args[0]
    resi = args[1]
    xquest_filenames = args[2:]

    for filename in xquest_filenames:
        xlink_set = pyxlinks.XlinksSet(filename)
        found = xlink_set.get_by_pos(prot, resi)
        if found:
            for x in found:
                xlink_set.print_xlink_csv(x, no_fieldnames=options.no_fieldnames)

if __name__ == '__main__':
    main()
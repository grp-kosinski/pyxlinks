import unittest
import os
import re
import tempfile

import Bio
import Bio.PDB

import pyxlinks

class TestPyxlinksFunctions(unittest.TestCase):
    @unittest.skip("get_unique now works only for xlink data")
    def test_get_unique_def_idfunc(self):
        in_list = [1, 8, 8, 4, 8, 4, 10, 1, 1]
        out_list = [1, 8, 4, 10]

        self.assertEqual(pyxlinks.get_unique(in_list), out_list)

    def test_mergeDataSets(self):
        pass

    @unittest.skip("get_unique now works only for xlink data")
    def test_get_unique_custom_idfunc(self):
        in_list = ['c', 'B', 'C', 'b']
        out_list = ['c', 'B']

        def idfun(x): return x.lower()

        self.assertEqual(pyxlinks.get_unique(in_list, idfun=idfun), out_list)

    @unittest.skip("_xlink2unique_marker_for_dist_restraints got deprecated")
    def test_xlink2unique_marker_for_dist_restraints_intra_xlink(self):
        data1 = {'Protein2': 'SCerevisiae_Elp1_NP_013488', 'Protein1': 'SCerevisiae_Elp1_NP_013488', 'TicA': '0.29', 'TicB': '0.17', 'Smallest_dist[euklid]': '', 'seriesscore': '16.4', 'Error_rel[ppm]': '-0.8', 'Type': 'xlink', 'TIC': '0.46', 'MatchOdds': '10.72', 'ld-Score': '44.57', 'deltaAA': '85', 'Error_abs[ppm]': '0.8', 'lApS': '-1.50901', 'AbsPos1': '1308', 'AbsPos2': '1223', 'Annotation': 'AO-A312_matched', 'xlinker': 'DSS', 'nseen': '20', 'XLType': 'intra-protein xl', 'wmatchoddsmean': '6.31', 'deltaS': '0.67', 'Mr': '2523.316', 'Mz': '842.113', 'intsum': '829', 'WTIC': '0.08', 'Xcorrb': '0.30', 'Spectrum': 'AO-A312.05380.05380.3_AO-A312.05359.05359.3', 'Rank': '1', 'Xcorrx': '0.39', 'fdr': '0.000', 'z': '3.000', 'wmatchoddssum': '3.16', 'Id': 'ANVKEIYSISEK-TGGTAKTGASR-a4-b6'}

        #data2 - as data1, just Protein1 and AbsPos1 inverted with Protein2 and AbsPos2, data2 should give the same marker as data1
        data2 = {'Protein1': 'SCerevisiae_Elp1_NP_013488', 'Protein2': 'SCerevisiae_Elp1_NP_013488', 'TicA': '0.29', 'TicB': '0.17', 'Smallest_dist[euklid]': '', 'seriesscore': '16.4', 'Error_rel[ppm]': '-0.8', 'Type': 'xlink', 'TIC': '0.46', 'MatchOdds': '10.72', 'ld-Score': '44.57', 'deltaAA': '85', 'Error_abs[ppm]': '0.8', 'lApS': '-1.50901', 'AbsPos2': '1308', 'AbsPos1': '1223', 'Annotation': 'AO-A312_matched', 'xlinker': 'DSS', 'nseen': '20', 'XLType': 'intra-protein xl', 'wmatchoddsmean': '6.31', 'deltaS': '0.67', 'Mr': '2523.316', 'Mz': '842.113', 'intsum': '829', 'WTIC': '0.08', 'Xcorrb': '0.30', 'Spectrum': 'AO-A312.05380.05380.3_AO-A312.05359.05359.3', 'Rank': '1', 'Xcorrx': '0.39', 'fdr': '0.000', 'z': '3.000', 'wmatchoddssum': '3.16', 'Id': 'ANVKEIYSISEK-TGGTAKTGASR-a4-b6'}

        out_marker = 'SCerevisiae_Elp1_NP_013488_1223_SCerevisiae_Elp1_NP_013488_1308_DSS'
        marker1 = pyxlinks._xlink2unique_marker_for_dist_restraints(data1)
        self.assertEqual(marker1, out_marker)

        marker2 = pyxlinks._xlink2unique_marker_for_dist_restraints(data2)
        self.assertEqual(marker2, out_marker)

    @unittest.skip("_xlink2unique_marker_for_dist_restraints got deprecated")
    def test_xlink2unique_marker_for_dist_restraints_inter_xlink(self):
        data1 = {'Protein2': 'SCerevisiae_Elp1_NP_013488', 'Protein1': 'SCerevisae_Elp3_NP_015239', 'TicA': '0.12', 'TicB': '0.33', 'Smallest_dist[euklid]': '', 'seriesscore': '32.5', 'Error_rel[ppm]': '-1.3', 'Type': 'xlink', 'TIC': '0.45', 'MatchOdds': 'Oct-73', 'ld-Score': '36.91', 'deltaAA': '238', 'Error_abs[ppm]': '1-Mar', 'lApS': '-0.00682', 'AbsPos1': '521', 'AbsPos2': '759', 'Annotation': 'AO-A320_matched', 'xlinker': 'DSS', 'nseen': '3', 'XLType': 'inter-protein xl', 'wmatchoddsmean': 'May-37', 'deltaS': '0.70', 'Mr': '2,449,228', 'Mz': '817,417', 'intsum': '441', 'WTIC': '0.08', 'Xcorrb': '0.27', 'Spectrum': 'AO-A320.05138.05138.3_AO-A320.05123.05123.3', 'Rank': '1', 'Xcorrx': '0.40', 'fdr': '0.000', 'z': '3,000', 'wmatchoddssum': 'Feb-69', 'Id': 'IAKEEHGSEK-YKEAFIVCR-a3-b2'}

        #data2 - as data1, just Protein1 and AbsPos1 inverted with Protein2 and AbsPos2, data2 should give the same marker as data1
        data2 = {'Protein1': 'SCerevisiae_Elp1_NP_013488', 'Protein2': 'SCerevisae_Elp3_NP_015239', 'TicA': '0.12', 'TicB': '0.33', 'Smallest_dist[euklid]': '', 'seriesscore': '32.5', 'Error_rel[ppm]': '-1.3', 'Type': 'xlink', 'TIC': '0.45', 'MatchOdds': 'Oct-73', 'ld-Score': '36.91', 'deltaAA': '238', 'Error_abs[ppm]': '1-Mar', 'lApS': '-0.00682', 'AbsPos2': '521', 'AbsPos1': '759', 'Annotation': 'AO-A320_matched', 'xlinker': 'DSS', 'nseen': '3', 'XLType': 'inter-protein xl', 'wmatchoddsmean': 'May-37', 'deltaS': '0.70', 'Mr': '2,449,228', 'Mz': '817,417', 'intsum': '441', 'WTIC': '0.08', 'Xcorrb': '0.27', 'Spectrum': 'AO-A320.05138.05138.3_AO-A320.05123.05123.3', 'Rank': '1', 'Xcorrx': '0.40', 'fdr': '0.000', 'z': '3,000', 'wmatchoddssum': 'Feb-69', 'Id': 'IAKEEHGSEK-YKEAFIVCR-a3-b2'}

        out_marker = 'SCerevisae_Elp3_NP_015239_521_SCerevisiae_Elp1_NP_013488_759_DSS'
        marker1 = pyxlinks._xlink2unique_marker_for_dist_restraints(data1)
        self.assertEqual(marker1, out_marker)

        marker2 = pyxlinks._xlink2unique_marker_for_dist_restraints(data2)
        self.assertEqual(marker2, out_marker)

class TestXlinksSet(unittest.TestCase):
    @unittest.skip("_xlink2unique_marker_for_dist_restraints got deprecated")    
    def test_get_unique_for_dist_restraints(self):
        data1 = {'Protein2': 'SCerevisiae_Elp1_NP_013488', 'Protein1': 'SCerevisae_Elp3_NP_015239', 'TicA': '0.12', 'TicB': '0.33', 'Smallest_dist[euklid]': '', 'seriesscore': '32.5', 'Error_rel[ppm]': '-1.3', 'Type': 'xlink', 'TIC': '0.45', 'MatchOdds': 'Oct-73', 'ld-Score': '36.91', 'deltaAA': '238', 'Error_abs[ppm]': '1-Mar', 'lApS': '-0.00682', 'AbsPos1': '521', 'AbsPos2': '759', 'Annotation': 'AO-A320_matched', 'xlinker': 'DSS', 'nseen': '3', 'XLType': 'inter-protein xl', 'wmatchoddsmean': 'May-37', 'deltaS': '0.70', 'Mr': '2,449,228', 'Mz': '817,417', 'intsum': '441', 'WTIC': '0.08', 'Xcorrb': '0.27', 'Spectrum': 'AO-A320.05138.05138.3_AO-A320.05123.05123.3', 'Rank': '1', 'Xcorrx': '0.40', 'fdr': '0.000', 'z': '3,000', 'wmatchoddssum': 'Feb-69', 'Id': 'IAKEEHGSEK-YKEAFIVCR-a3-b2'}

        #data2 - as data1, just Protein1 and AbsPos1 inverted with Protein2 and AbsPos2, data2 should give the same marker as data1
        data2 = {'Protein1': 'SCerevisiae_Elp1_NP_013488', 'Protein2': 'SCerevisae_Elp3_NP_015239', 'TicA': '0.12', 'TicB': '0.33', 'Smallest_dist[euklid]': '', 'seriesscore': '32.5', 'Error_rel[ppm]': '-1.3', 'Type': 'xlink', 'TIC': '0.45', 'MatchOdds': 'Oct-73', 'ld-Score': '36.91', 'deltaAA': '238', 'Error_abs[ppm]': '1-Mar', 'lApS': '-0.00682', 'AbsPos2': '521', 'AbsPos1': '759', 'Annotation': 'AO-A320_matched', 'xlinker': 'DSS', 'nseen': '3', 'XLType': 'inter-protein xl', 'wmatchoddsmean': 'May-37', 'deltaS': '0.70', 'Mr': '2,449,228', 'Mz': '817,417', 'intsum': '441', 'WTIC': '0.08', 'Xcorrb': '0.27', 'Spectrum': 'AO-A320.05138.05138.3_AO-A320.05123.05123.3', 'Rank': '1', 'Xcorrx': '0.40', 'fdr': '0.000', 'z': '3,000', 'wmatchoddssum': 'Feb-69', 'Id': 'IAKEEHGSEK-YKEAFIVCR-a3-b2'}

        xlinks_data = [
            data1,
            data2
        ]

        xlinks_set = pyxlinks.XlinksSet(xlink_set_data=xlinks_data, fieldnames=list(data1.keys()))
        self.assertEqual(len(xlinks_set.data), 2)
        unique_xlinks_set = xlinks_set.get_unique_set_for_dist_restraints()

        self.assertEqual(len(unique_xlinks_set.data), 1)




class XlinkAnalyzerTest(unittest.TestCase):
    def setUp(self):
        self.example = 'PolI' #used for output files

        self.data_config_filename = 'PolI/PolI_data_cfg.json'

        self.components = ['A190', 'A135', 'ABC10beta', 'ABC10alpha', 'AC40', 'AC19', 'A12', 'A34.5', 'A49', 'ABC27', 'A14', 'A43', 'ABC23', 'ABC14.5']

        self.seq = 'MSNIVGIEYNRVTNTTSTDFPGFSKDAENEWNVEKFKKDFEVNISSLDAREANFDLINID\
TSIANAFRRIMISEVPSVAAEYVYFFNNTSVIQDEVLAHRIGLVPLKVDPDMLTWVDSNL\
PDDEKFTDENTIVLSLNVKCTRNPDAPKGSTDPKELYNNAHVYARDLKFEPQGRQSTTFA\
DCPVVPADPDILLAKLRPGQEISLKAHCILGIGGDHAKFSPVSTASYRLLPQINILQPIK\
GESARRFQKCFPPGVIGIDEGSDEAYVKDARKDTVSREVLRYEEFADKVKLGRVRNHFIF\
NVESAGAMTPEEIFFKSVRILKNKAEYLKNCPITQ'
        
        self.pdb_filename = 'PolI/4C3I.pdb'

        self.distances = [
            (('A',5),('A',70),19.500),
            (('A',1012),('B',1043),28.545)
        ]

    def test_init(self):
        xlinkAnalyzer = pyxlinks.XlinkAnalyzer(self.data_config_filename)

        self.assertEqual(xlinkAnalyzer.cfg.components, self.components)
        

    def test_make_xlinked_peptides_for_all_seqs(self):
        xlinkAnalyzer = pyxlinks.XlinkAnalyzer(self.data_config_filename)

        missed_cleavages = 1
        min_pept_length = None
        max_pept_length = None
        xlinkAnalyzer.make_xlinked_peptides_for_all_seqs(missed_cleavages, min_pept_length, max_pept_length)


    def test_make_xlinked_peptides(self):
        peptides = pyxlinks.make_xlinked_peptides(self.seq, missed_cleavages=1, min_pept_length=None, max_pept_length=None, rule='[KR](?=[^P])')

        # print self.seq
        # print peptides.ordered_list

        for pept in peptides.ordered_list:
            ref_pept = self.seq[pept['start_resi']-1:pept['start_resi']-1+len(pept['seq'])]
            self.assertEqual(ref_pept, pept['seq'])
            for mod_pos in pept['mod_pos']:
                self.assertEqual('K', pept['seq'][mod_pos])

    def test_make_xlinked_peptides_for_all_seqs(self):
        missed_cleavages = 1
        xlinkAnalyzer = pyxlinks.XlinkAnalyzer(self.data_config_filename)
        xlinkAnalyzer.make_xlinked_peptides_for_all_seqs(missed_cleavages=missed_cleavages, min_pept_length=None, max_pept_length=None)
        xlinkAnalyzer.load_xlinks()
        xlinkAnalyzer.test_theoretical_peptides()



        for comp, peptides in xlinkAnalyzer.theoretical_peptides.items():
            full_seq = xlinkAnalyzer.cfg.get_seq(comp)
            for pept in peptides.ordered_list:
                test_peptide = pept['seq']
                missed_cleavages_found = len(list(re.finditer(xlinkAnalyzer.cleavage_rule, test_peptide))) - len(pept['mod_pos'])
                # print pept, missed_cleavages
                self.assertGreaterEqual(missed_cleavages, missed_cleavages_found)
                self.assertNotEqual(-1, full_seq.find(pept['seq']))

    def test_gen_xlinks_possible_in_structure(self):
        '''Test the ContactMatrix generation and getting the list of contacts of crosslikable pairs'''
        pdb_structure = Bio.PDB.PDBParser().get_structure(self.pdb_filename, self.pdb_filename)
        missed_cleavages = 1
        xlink_possible_fn = lambda r1, r2: pyxlinks.is_pair_crosslinkable(r1, r2, biodssp=None, acc_thresh=None)
        xlinkAnalyzer = pyxlinks.XlinkAnalyzer(self.data_config_filename)
        xlinkAnalyzer.make_contact_matrix(pdb_structure, possible_fn=xlink_possible_fn)
        xlinkAnalyzer.gen_xlinks_possible_in_structure(xlink_len_threshold=30)


        out_lines = ['open ' + self.pdb_filename]
        for xlink in xlinkAnalyzer.possible_xlinks:
            out_lines.append('distance :{resi1}.{chain1}@CA :{resi2}.{chain2}@CA'.format(chain1=xlink[0][0], resi1=xlink[0][1], chain2=xlink[1][0], resi2=xlink[1][1]))


        log_filename = os.path.join(tempfile.gettempdir(), '{0}_{1}.cmd'.format(self.example, self._testMethodName))
        with open(log_filename, 'w') as f:
            f.write('\n'.join(out_lines))

        for dist in self.distances:
            self.assertAlmostEqual(xlinkAnalyzer.contactMatrix.matrix[dist[0][0], dist[0][1]][dist[1][0], dist[1][1]], dist[2], places=3)

class XlinkAnalyzerTest_Pol3(XlinkAnalyzerTest):
    def setUp(self):
        self.example = 'PolIII' #used for output files

        self.data_config_filename = 'PolIII/data_cfg.json'

        self.components = [
            "C160",
            "C128",

            "ABC10beta",
            "ABC10alpha",
            "AC40",
            "AC19",

            "C11",
            "C53",
            "C37",

            "C82",
            "C34",
            "C31",
            "ABC27",


            "C17",
            "C25",
            "ABC23",
            "ABC14.5"
        ]

#         self.seq = 'MKEVVVSETPKRIKGLEFSALSAADIVAQSEVEVSTRDLFDLEKDRAPKANGALDPKMGV\
# SSSSLECATCHGNLASCHGHFGHLKLALPVFHIGYFKATIQILQGICKNCSAILLSETDK\
# RQFLHELRRPGVDNLRRMGILKKILDQCKKQRRCLHCGALNGVVKKAAAGAGSAALKIIH\
# DTFRWVGKKSAPEKDIWVGEWKEVLAHNPELERYVKRCMDDLNPLKTLNLFKQIKSADCE\
# LLGIDATVPSGRPETYIWRYLPAPPVCIRPSVMMQDSPASNEDDLTVKLTEIVWTSSLIK\
# AGLDKGISINNMMEHWDYLQLTVAMYINSDSVNPAMLPGSSNGGGKVKPIRGFCQRLKGK\
# QGRFRGNLSGKRVDFSGRTVISPDPNLSIDEVAVPDRVAKVLTYPEKVTRYNRHKLQELI\
# VNGPNVHPGANYLLKRNEDARRNLRYGDRMKLAKNLQIGDVVERHLEDGDVVLFNRQPSL\
# HRLSILSHYAKIRPWRTFRLNECVCTPYNADFDGDEMNLHVPQTEEARAEAINLMGVKNN\
# LLTPKSGEPIIAATQDFITGSYLISHKDSFYDRATLTQLLSMMSDGIEHFDIPPPAIMKP\
# YYLWTGKQVFSLLIKPNHNSPVVINLDAKNKVFVPPKSKSLPNEMSQNDGFVIIRGSQIL\
# SGVMDKSVLGDGKKHSVFYTILRDYGPQEAANAMNRMAKLCARFLGNRGFSIGINDVTPA\
# DDLKQKKEELVEIAYHKCDELITLFNKGELETQPGCNEEQTLEAKIGGLLSKVREEVGDV\
# CINELDNWNAPLIMATCGSKGSTLNVSQMVAVVGQQIISGNRVPDGFQDRSLPHFPKNSK\
# TPQSKGFVRNSFFSGLSPPEFLFHAISGREGLVDTAVKTAETGYMSRRLMKSLEDLSCQY\
# DNTVRTSANGIVQFTYGGDGLDPLEMEGNAQPVNFNRSWDHAYNITFNNQDKGLLPYAIM\
# ETANEILGPLEERLVRYDNSGCLVKREDLNKAEYVDQYDAERDFYHSLREYINGKATALA\
# NLRKSRGMLGLLEPPAKELQGIDPDETVPDNVKTSVSQLYRISEKSVRKFLEIALFKYRK\
# ARLEPGTAIGAIGAQSIGEPGTQMTLKTFHFAGVASMNVTLGVPRIKEIINASKVISTPI\
# INAVLVNDNDERAARVVKGRVEKTLLSDVAFYVQDVYKDNLSFIQVRIDLGTIDKLQLEL\
# TIEDIAVAITRASKLKIQASDVNIIGKDRIAINVFPEGYKAKSISTSAKEPSENDVFYRM\
# QQLRRALPDVVVKGLPDISRAVINIRDDGKRELLVEGYGLRDVMCTDGVIGSRTTTNHVL\
# EVFSVLGIEAARYSIIREINYTMSNHGMSVDPRHIQLLGDVMTYKGEVLGITRFGLSKMR\
# DSVLQLASFEKTTDHLFDAAFYMKKDAVEGVSECIILGQTMSIGTGSFKVVKGTNISEKD\
# LVPKRCLFESLSNEAALKAN'

        self.seq = 'MSSNKGNGRLPSLKDSSSNGGGSAKPSLKFKPKAVARKSKEEREAAASKVKLEEESKRGN\
DKKHFNNKNKRVTGAGGQQRRMAKYLNNTHVISSGPLAAGNFVSEKGDLRRGFIKSEGSG\
SSLVQKGLETIDNGAESSENEAEDDDNEGVASKSKKKFNMGKEFEARNLIEDEDDGESEK\
SSDVDMDDEEWRSKRIEQLFPVRPVRVRHEDVETVKREIQEALSEKPTREPTPSVKTEPV\
GTGLQSYLEERERQVNEKLADLGLEKEFQSVDGKEAAAELELLNADHQHILRKLKKMNNK\
PERFMVFQLPTRLPAFERPAVKEEKEDMETQASDPSKKKKNIKKKDTKDALSTRELAGKV\
GSIRVHKSGKLSVKIGNVVMDIGKGAETTFLQDVIALSIADDASSAELLGRVDGKIVVTP\
QI'

        self.pdb_filename = 'PolIII/sol0.pdb'



        self.distances = [
            (('A',2),('A',11),14.409),
            (('O',520),('P',62),16.830)
        ]


if __name__ == '__main__':
    unittest.main()
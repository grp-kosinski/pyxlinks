source ~/bin/activate_python2.7_environment.sh
source /struct/cmueller/kosinski/software/ENV2.7/bin/activate

python ../../../scripts/expected_from_pdb.py -t 30 -m 1 -d PolI_data_cfg.json 4C3I.pdb expected_from_pdb.out.csv

python /struct/cmueller/kosinski/devel/pyxlinks/scripts/make_stats.py out.csv
